import React, { useEffect, useState } from 'react';
import './App.css';

function App() {
  const [boardLayout, setBoardLayout] = useState([[]]);
  const [wolf, setWolf] = useState({});
  const [thomas, setThomas] = useState({});
  const [message, setMessage] = useState(null);

  useEffect(()=>{
    resetBoard();
  },[]);

  const resetBoard = async () => {
    const dataObj = await fetch('/start');
    const newData = await dataObj.json();
    setBoardLayout(newData.layout);
    setWolf(newData.wolf);
    setThomas(newData.thomas);
    setMessage(newData.message);
  }

  const restart = async () => { await resetBoard(); };

  const gameAction = (x,y) => async () => {
    const dataObj = await fetch('/move', {
      method: 'post',
      body: JSON.stringify({x,y})
    });
    const newData = await dataObj.json();
    setWolf(newData.wolf);
    setThomas(newData.thomas);
    setMessage(newData.message);
  };

  const getPerson = (x,y) => {
    if (wolf.column===x && wolf.row===y) {
      return 'X';
    }
    if (thomas.column===x && thomas.row===y) {
      return 'O';
    }
    return null
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <div>
            <table>
              {boardLayout.map((row, rowNum)=>(
                  <tr>
                    {row.map((cell, colNum)=>{
                      return (
                          <td className={`cell ${cell.split('').join(' ')}`}>
                            {getPerson(colNum+1, rowNum+1)}
                          </td>
                      );
                    })}
                  </tr>
              ))}
            </table>
            <div>
              <button onClick={gameAction(-1,0)}>Left</button>
              <button onClick={gameAction(1,0)}>Right</button>
              <button onClick={gameAction(0,-1)}>Up</button>
              <button onClick={gameAction(0,1)}>Down</button>
              <button onClick={gameAction(0,0)}>Skip</button>
            </div>
          </div>
        </div>
        <br/>
        {message}
        <br/>
        <button
          rel="noopener noreferrer"
          onClick={restart}
        >
          Reset Game
        </button>
      </header>
    </div>
  );
}

export default App;
