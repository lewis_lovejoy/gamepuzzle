# Thomas and the wolf

So I decided to do the second type of problem - providing an API for
playing the game interactively. So there are two parts:

- The nodejs server - providing two express endpoints for playing and reseting

- The client part - A simple ReactJS app

The focus has been on the server part - the React part is just a quick example to show the
API working. puzzlePlayer.js is where the action is - so if you just want to look at code 
without running then thats where you want to be.

The main negative of this solution is the use of global variables as the 
'state' of the game in the puzzlePlayer module - anyone using the API will essentially be 
playing the same game. In real life this would need a user/game token for
each game - and some storage of this state (in memory, on disk etc..) - but given 
this is a simple example I've left it.

To run:

 - Load the server (on port 5000)
    - `cd server`
    - `npm install`
    - `npm start`
- Load the client dev in dev mode (on port 3000)
    - `cd client`
    - `npm install`
    - `npm start`
    - point a browser at http://localhost:3000/

### Some notes

In no particular order:

- The client is dirty - just a basic 'create react' with the App.js updated to show a board with buttons. (No tests etc.)
- The server code has a small number of tests - but not even close to exhaustive (or TDD)
- I just load the example puzzle json using 'require' - so its pretty hardcoded to this game (e.g. changing the JSON requires the server to be restarted)
- I've hardcoded to use the first puzzle in the json ONLY as well
- The ambiguous direction if wolf is not on the same row or column - is just hard coded
- I've not looked at any performance issues - i.e. optimised any algorithms - this is built for readability/maintenance over performance
- The EXIT algorithm is not quite as per the spec - Thomas has to pass through the exit (rather than just land on the box next to the exit). This just saved me calculating all the exit squares (given there could be more than one) - just leaving the grid means you're out
- There is no error handling (anywhere), it's just the basics
- I'd normally run babel etc to allow node to have access to more modern ES6+ features (like 'import' etc..)
- The API endpoints are basic - 'start' and 'move' as GET and POST on /start and /move (and the GET should probably be a POST as it starts the game as well as returning it). Its not the best API
- If thomas tries to move into a wall - his move is rejected and he gets to try again
- I preprocess the JSON layout into a 2d Array - which speeds up all the 'lookups' but does use memory
- No refactoring to simplify code or tidy up - it's a v1 - it works, but a B+ at best
- The 4 global variables cause me pain (I could have at least made them into an object)
- All on a single commit on master as well

Given time I'd add an 'auto' button to play for you (solving the first part
of the problem spec as well).