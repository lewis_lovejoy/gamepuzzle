const express = require('express');
const bodyParser = require('body-parser');
const startPuzzle = require('./PuzzleSetups.json');
const {
	resetLayout,
	getCurrentLayout,
	getCurrentPlayers,
	moveThomas,
	moveWolf
} = require('./puzzlePlayer.js');

const port = 5000;
const app = express();
const textParser = bodyParser.text();

app.get('/start', async (req, res) => {
	resetLayout(startPuzzle);
	res.send({
		layout: getCurrentLayout(),
		...getCurrentPlayers()
	});
})

app.post('/move', textParser, async (req, res) => {
	const { x, y } = JSON.parse(req.body);
	if (!moveThomas(x,y)) {
		res.send(getCurrentPlayers());
		return;
	}
	moveWolf();
	res.send(getCurrentPlayers());
})

app.listen(port, () => {
	console.log(`Game Server app listening at http://localhost:${port}`)
})

resetLayout(startPuzzle);