let currentLayout;
let wolf;
let thomas;
let message = null;

const isWallInWay = (x,y,borders) => {
	if (x > 0 && borders.indexOf('R') > -1) {
		return true;
	}
	if (x < 0 && borders.indexOf('L') > -1) {
		return true;
	}
	if (y > 0 && borders.indexOf('B') > -1) {
		return true;
	}
	if (y < 0 && borders.indexOf('T') > -1) {
		return true;
	}
	return false;
}

const getStep = (a,b) => Math.sign(b-a );

const moveWolfForReal = () => {
	const bordersOnWolf = currentLayout[wolf.row-1][wolf.column-1];
	let x=0,y=0;
	if (wolf.row===thomas.row) {
		x = getStep(wolf.column, thomas.column);
		if (isWallInWay(x,y,bordersOnWolf)) {
			return;
		}
	}
	else if (wolf.column===thomas.column) {
		y = getStep(wolf.row, thomas.row);
		if (isWallInWay(x,y,bordersOnWolf)) {
			return;
		}
	}
	else {
		x = getStep(wolf.column, thomas.column);
		if (isWallInWay(x,y,bordersOnWolf)) {
			x = 0;
			y = wolf.row > thomas.row ? -1 : +1;
			if (isWallInWay(x,y,bordersOnWolf)) {
				return;
			}
		}
	}

	wolf.row += y;
	wolf.column += x;
}

exports.resetLayout = (startPuzzle) => {
	const { puzzles = [] } = startPuzzle;
	const puzzle = puzzles[0];
	const { layout } = puzzle;
	const numRows = layout.reduce((acc,item)=>Math.max(acc,item.row),0);
	currentLayout = Array.apply(null, Array(numRows)).map(() => ([]));

	layout.map((item)=>{
		currentLayout[item.row-1][item.column-1] = item.borders;
	});

	wolf = { ...puzzle.wolf };
	thomas = { ...puzzle.thomas };
	message = null;
}

exports.moveThomas = (x,y) => {
	const bordersOnThomas = currentLayout[thomas.row-1][thomas.column-1];
	if (isWallInWay(x,y,bordersOnThomas)) {
		message = 'Wall in the way';
		return false;
	}

	thomas.row += y;
	thomas.column += x;

	if (!currentLayout[thomas.row-1] || thomas.column < 0 || currentLayout[thomas.row-1].length < thomas.column) {
		message = 'You GOT out';
		return false;
	}
	message = null;
	return true;
}

exports.moveWolf = () => {
	let times = 0;
	while (times < 2) {
		moveWolfForReal();
		if (wolf.row === thomas.row && wolf.column === thomas.column) {
			message = 'You got caught by the wolf';
			return;
		}
		times++;
	}
}

exports.getCurrentLayout = () =>  currentLayout;
exports.getCurrentPlayers = () => ({wolf, thomas, message})
