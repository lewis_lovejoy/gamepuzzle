const {
	resetLayout,
	getCurrentLayout,
	getCurrentPlayers,
	moveThomas,
	moveWolf
} = require('./puzzlePlayer.js');

describe('test a simple puzzle', () => {
	const puzzle = {
		"puzzles": [
			{
				"name": "puzzle1",
				"layout": [
					{ "row": 1, "column": 1, "borders": "LT" },
					{ "row": 1, "column": 2, "borders": "TR" },
					{ "row": 2, "column": 1, "borders": "LB" },
					{ "row": 2, "column": 2, "borders": "RB" },
				],
				"wolf": { "row": 2, "column": 1 },
				"thomas": { "row": 2, "column": 2 }
			}
		]
	};

	beforeEach(() => {
		resetLayout(puzzle);
	});

	test('Grid is correct', () => {
		expect(getCurrentLayout()).toEqual( [["LT", "TR"], ["LB", "RB"]]);
	});

	test('PLayers are in correct place', () => {
		expect(getCurrentPlayers()).toEqual({"message": null, "thomas": {"column": 2, "row": 2}, "wolf": {"column": 1, "row": 2}});
	});

	test('Man can move left', () => {
		moveThomas(-1,0);
		expect(getCurrentPlayers()).toEqual({"message": null, "thomas": {"column": 1, "row": 2}, "wolf": {"column": 1, "row": 2}});
	});

	test('Man can move up', () => {
		moveThomas(0,-1);
		expect(getCurrentPlayers()).toEqual({"message": null, "thomas": {"column": 2, "row": 1}, "wolf": {"column": 1, "row": 2}});
	});

	test('Man cant move up right through wall', () => {
		moveThomas(1,0);
		expect(getCurrentPlayers()).toEqual({"message": "Wall in the way", "thomas": {"column": 2, "row": 2}, "wolf": {"column": 1, "row": 2}});
	});


	test('Wolf eats Thomas', () => {
		moveWolf();
		expect(getCurrentPlayers()).toEqual({"message": "You got caught by the wolf", "thomas": {"column": 2, "row": 2}, "wolf": {"column": 2, "row": 2}});
	});

});

